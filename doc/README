==================================
  pgHA  README file
==================================
 
  * Overview
  * Installation
  * Systems
  * Usage
  * Logging
  * Stopping Failoverd
  

============
  Overview
============

    Failoverd is an automatic failover monitoring system for 
    Postgres database systems.  It is designed to work with 
    pgBouncer installed on the application servers for simplicity.

================
  Installation
================
    
    1 Setup machines:
        - Master DB
        - Slave DB
        - Application server

    2 Ensure that there is full network connectivity between all the servers

    3 Setup postgres streaming replication between the master and slave DB's
        - See the 'masterDB' and 'slaveDB' directories in the failoverd 'doc' 
          distribution for config files to assist with this.

    4 Create a table in the master (which will propagate to the slave):
        - See cfg/status_table.sql for DDL

    5 Install pgBouncer on your application server
        - Precompiled for linux x86_64 included
        - Create a new user on the app server called 'bouncer'
        - Setup ssh keys for the cluster manager to talk to pgBouncer
                - On the slave DB server, as postgres user
                        $ ssh-keygen -t rsa
                        ( accept all defaults, provide no passphrase ) 
                - Copy the contents of ~/.ssh/id_rsa.pub to the app server
                - On the appserver, as the bouncer user:
                        $ mkdir ~/.ssh
                        $ chmod 0700 .ssh
                        $ vi authorized_keys
                        ( Paste the contents of id_rsa.pub from the slave DB)
                        ( Save and close the file )
                        $ chmod 0600 .ssh/authorized_keys 

        - Setup a pgbouncer.ini.orig for the master DB
        - Setup a pgbouncer.ini.failover for the failover DB 
		( See support/pgBouncer/cfg/ for example config files ) 

	- Once those are setup, cp pgbouncer.ini.orig pgbouncer.ini
        - Define these config files in failoverd's config file

    6 Extract the pgHA.zip file:
        - cd /opt/
        - unzip pgHA.zip

    -----------------

    pgHA uses a few perl modules that must be install prior to execution.  
    If your master and slave servers have access to the internet, you may 
    execute the following commands to install these successfully:

    perl -MCPAN -e 'install Config::IniFiles'
    perl -MCPAN -e 'install DBI'
    perl -MCPAN -e 'install DBD::Pg'
       -- Note, if asked for the path to 'pg_config' simply enter the full
          path to your postgres bin/pg_config e.g.:
           /opt/postgres/9.2/bin/pg_config
    perl -MCPAN -e 'install Log::Log4perl'
    perl -MCPAN -e 'install Proc::Daemon'
    perl -MCPAN -e 'install Net::Ping'

    Config::IniFiles;
    Date::Format;
    DBI;
    DBD::Pg;
    File::Basename;
    Getopt::Long;
    IO::Socket::INET;
    Log::Log4perl;
    Proc::Daemon;
    Net::Ping;

    Running the failoverd executable with no options will fail if these are not installed properly.

=================
    Systems
=================

    pgBouncer
    =========
        Failoverd uses a single daemon running on the slave database server to manage
        failure scenario detection.  In order for this solution to effectively detect 
        if the application has been impacted, as well as provide an 'auto-failover' 
        mechanism, you need to install pgbouncer (http://pgbouncer.projects.postgresql.org )
        on your application servers.  You should configure your application's data-source to 
        point to 127.0.0.1 for all of your databases and have pgbouncer manage the connections
        to the actual postgres database systems.

        Without customization, Failoverd cannot be used without pgbouncer being in the environment.

    Instance 
    ========
        When started, failoverd will kick off a background daemon that will be used to monitor 
        the master database.  This is referred to as an 'instance'.  Each instance of failoverd 
        should be used to automate failover for one and only one master-slave database pair.  

    Monitoring
    ==========
        Once started, failoverd will be monitorable in multiple ways.  The simplest is to use 
            ./failoverd.pl -c <path/to/config> --status

        You may also monitor by running:
            ps aux | grep failoverd 

         Of course, the log file is very detailed as well.  The location of the log file is set
         in your 'log4perl.conf' in the cfg directory.


=================
  Configuration
=================

    Once you have extracted the distribution, you will need to tailor failoverd for your environment

    * Instance Config : In the cfg directory, use the 'example.conf' file to 
                        tailor a configuration for your environment.  The example.conf
                        file is well-commented and should be used as the source for 
                        configuration documentation.

    * log4perl.conf   : Set the path to the log file.


============
  Usage
============

    Once you have created your config file, you can start failoverd by:

     ./failoverd.pl -c ../cfg/prod_billing_cluster.conf --start

    If an instance of failoverd is already running for that instance
    failoverd will exit, otherwise it will spawn a background daemon
    and begin monitoring.

  
============
  Logging
============

    Failoverd uses log4perl (similar to log4j) to define log levels.  The
    master log file contains all the information needed to see the health of 
    the system.  The two levels of logging available are:

        * INFO  ( recommended for prod ) 
        * DEBUG ( only useful for developing against failoverd )

    The full path to your log4perl.conf file is set in the 'instance' config (see example.conf).
    The full path to the actual log file is defined in log4perl.conf itself

=======================
  Stopping Failoverd
=======================

    To stop Failoverd, you can use the --stop option to the program:

        ./failoverd.pl -c ../cfg/prod_billing_cluster.conf --stop

